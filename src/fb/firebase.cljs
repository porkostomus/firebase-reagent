(ns fb.app.firebase
  (:require [clojure.string :as string]))

(defn init []
 (js/firebase.initializeApp
   #js {:apiKey "AIzaSyCwWxzPB60nnFYTIGIQmPOb_7O3YG1GMps"
        :authDomain "blog-b2952.firebaseapp.com"
        :databaseURL "https://blog-b2952.firebaseio.com"
        :storageBucket "blog-b2952.appspot.com"}))

(defn db-ref [path]
 (.ref (js/firebase.database) (string/join "/" path)))

(defn save [path value]
 (.set (db-ref path) value))

(defn load [path]
  (.once
   (db-ref path)
   "value"
   (fn received-db [snapshot]
     (prn "Got:" (.val snapshot)))))
